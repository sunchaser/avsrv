<?php

class WebUser extends CWebUser
{
	/**
	 * @var User
	 */
	private $model;

	/**
	 * @return User
	 */
	public function getModel()
	{
		if ($this->model === null)
		{
			$cred = Credential::model()->findByAttributes(['value' => $this->id]);
			if ($cred)
			{
				$this->model = $cred->user;
			}
		}

		return $this->model;
	}
}
