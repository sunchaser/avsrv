<?php
/* @var $this SiteController */
/* @var $pwd_form PasswordForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Password Change';
$this->breadcrumbs=array(
	'Password Change',
);
?>

<h1>Password Change</h1>

<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'password-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php echo $form->labelEx($pwd_form,'password'); ?>
		<?php echo $form->passwordField($pwd_form,'password'); ?>
		<?php echo $form->error($pwd_form,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($pwd_form,'new_password'); ?>
		<?php echo $form->passwordField($pwd_form,'new_password'); ?>
		<?php echo $form->error($pwd_form,'new_password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($pwd_form,'confirmation'); ?>
		<?php echo $form->passwordField($pwd_form,'confirmation'); ?>
		<?php echo $form->error($pwd_form,'confirmation'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Save'); ?>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->
