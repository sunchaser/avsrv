<?php
/* @var $this ImagesController */
/* @var $data Image */
?>

<div class="view" style="display: inline-block;">

	<b>
	<?php echo CHtml::link(CHtml::image(Yii::app()->createUrl('images/thumbnail', ['hash' => $data->hash])), array('view', 'id'=>$data->id)); ?>
	<br />

</div>
