<?php
/* @var $this CredentialsController */
/* @var $data Credential */
?>

<div class="view">

	<?php if ($data->type === Credential::TYPE_EMAIL): ?>
		<b>Email:</b>
	<?php else: ?>
		<b>OpenID:</b>
	<?php endif ?>
	<?= CHtml::link(CHtml::encode($data->value), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image_id')); ?>:</b>
	<?= CHtml::link($data->image_id ? $data->image->imgThumbnailCode : 'No Image', ['credentials/associate', 'c' => $data->id]); ?>
	<br />

</div>
