<?php
/* @var $this CredentialsController */
/* @var $model Credential */

$this->breadcrumbs=array(
	'Credentials'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Credential', 'url'=>array('index')),
	array('label'=>'Create Credential', 'url'=>array('create')),
	array('label'=>'Update Credential', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Credential', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Credential', 'url'=>array('admin')),
);
?>

<h1>View Credential #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'value',
		'type',
		'image_id',
		'md5',
		'sha256',
	),
)); ?>

<?= CHtml::image(Yii::app()->createUrl('avatar/avatar', ['hash' => $model->sha256, 'size' => 32])) ?>
<?= CHtml::image(Yii::app()->createUrl('avatar/avatar', ['hash' => $model->sha256, 'size' => 80])) ?>
<?= CHtml::image(Yii::app()->createUrl('avatar/avatar', ['hash' => $model->sha256, 'size' => 150])) ?>
