<?php
/**
 * @var Credential $cred
 * @var Image[] $images
 */
?>

<div>
	Set image for <?= $cred->type === Credential::TYPE_EMAIL ? 'Email' : 'OpenID' ?> <?= $cred->value ?>
</div>

<div>
	<?php foreach ($images as $i): ?>
		<div style="float:left; padding: ">
			<?= CHtml::link($i->imgThumbnailCode, ['credentials/associate', 'c' => $cred->id, 'i' => $i->id]) ?>
		</div>
	<?php endforeach ?>
</div>
