<?php
/* @var $this CredentialsController */
/* @var $model Credential */

$this->breadcrumbs=array(
	'Credentials'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Credential', 'url'=>array('index')),
	array('label'=>'Manage Credential', 'url'=>array('admin')),
);
?>

<h1>Create Credential</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>