<?php
/* @var $this CredentialsController */
/* @var $model Credential */

$this->breadcrumbs=array(
	'Credentials'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Credential', 'url'=>array('index')),
	array('label'=>'Create Credential', 'url'=>array('create')),
	array('label'=>'View Credential', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Credential', 'url'=>array('admin')),
);
?>

<h1>Update Credential <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>