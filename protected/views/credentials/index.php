<?php
/* @var $this CredentialsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Credentials',
);

$this->menu=array(
	array('label'=>'Create Credential', 'url'=>array('create')),
	array('label'=>'Manage Credential', 'url'=>array('admin')),
);
?>

<h1>Credentials</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
