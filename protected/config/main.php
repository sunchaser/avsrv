<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return [
	'basePath'   => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name'       => 'Avatar Server',

	// preloading 'log' component
	'preload'    => ['log', 'bootstrap'],

	// autoloading model and component classes
	'import'     => [
		'application.models.*',
		'application.components.*',
	],

	'modules'    => [
		// uncomment the following to enable the Gii tool
		'gii' => [
			'class'     => 'system.gii.GiiModule',
			'password'  => 'avsrv',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters' => ['127.0.0.1', '::1'],
		],
	],

	// application components
	'components' => [
		'user'         => [
			// enable cookie-based authentication
			'class'          => 'WebUser',
			'allowAutoLogin' => true,
		],
		'urlManager'   => [
			'urlFormat'      => 'path',
			'showScriptName' => false,
			'rules'          => [
				'avatar/<hash:[\da-f]{32}>'                      => 'avatar/avatar',
				'avatar/<hash:[\da-f]{64}>'                      => 'avatar/avatar',
				'control/<controller:\w+>/<id:\d+>'              => '<controller>/view',
				'control/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'control/<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
			],
		],
		'db'           => [
			'connectionString' => 'pgsql:host=localhost;dbname=avsrv',
			'username'         => 'avsrv',
			'password'         => 'avsrv',
			'emulatePrepare'   => true,
		],
		'errorHandler' => [
			// use 'site/error' action to display errors
			'errorAction' => 'site/error',
		],
		'log'          => [
			'class'  => 'CLogRouter',
			'routes' => [
				[
					'class'  => 'CFileLogRoute',
					'levels' => 'error, warning',
				],
				// uncomment the following to show log messages on web pages
				/*
				[
					'class'=>'CWebLogRoute',
				],
				*/
			],
		],
	],

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'     => [
		// this is used in contact page
		'adminEmail' => 'webmaster@example.com',
		'timezone'   => 'UTC',
	],
];
