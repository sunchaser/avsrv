<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $password
 * @property string $reg_date
 * @property integer $role
 *
 * The followings are the available model relations:
 * @property Credential[] $credentials
 * @property Image[] $images
 */
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('password, reg_date', 'required'),
			array('role', 'numerical', 'integerOnly' => true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, password, reg_date, role', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'credentials' => array(self::HAS_MANY, 'Credential', 'user_id'),
			'images'      => array(self::HAS_MANY, 'Image', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'       => 'ID',
			'password' => 'Password',
			'reg_date' => 'Reg Date',
			'role'     => 'Role',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('password', $this->password, true);
		$criteria->compare('reg_date', $this->reg_date, true);
		$criteria->compare('role', $this->role);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function checkPassword($password)
	{
		// use PHP 5.5 password API
		if (password_verify($password, $this->password))
		{
			if (password_needs_rehash($this->password, PASSWORD_DEFAULT))
			{
				$this->saveAttributes([
					'password' => password_hash($password, PASSWORD_DEFAULT),
				]);
			}

			return true;
		}

		// simple standard for manual db maintenance
		if ('pwd_' . $password === $this->password)
		{
			// definitely needs rehash
			$this->saveAttributes([
				'password' => password_hash($password, PASSWORD_DEFAULT),
			]);

			return true;
		}

		return false;
	}

	public function setPassword($password)
	{
		$this->password = password_hash($password, PASSWORD_DEFAULT);
	}
}
