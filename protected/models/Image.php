<?php

/**
 * This is the model class for table "images".
 *
 * The followings are the available columns in table 'images':
 * @property integer $id
 * @property integer $user_id
 * @property string $hash
 * @property string $file
 *
 * @property string $imgThumbnailCode
 *
 * The followings are the available model relations:
 * @property Credential[] $credentials
 * @property User $user
 */
class Image extends CActiveRecord
{
	/**
	 * @var CUploadedFile
	 */
	public $image; // file for upload

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Image the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'images';
	}

	public function getImgThumbnailCode()
	{
		return CHtml::image(Yii::app()->createUrl('images/thumbnail', ['hash' => $this->hash]));
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			['user_id, hash, file, image', 'required'],
			['user_id', 'numerical', 'integerOnly' => true],
			['image', 'file', 'types' => 'jpg, gif, png, jpeg'],
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			['id, user_id, hash, file', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'credentials' => array(self::HAS_MANY, 'Credential', 'image_id'),
			'user'        => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'      => 'ID',
			'user_id' => 'User',
			'hash'    => 'Hash',
			'file'    => 'File',
		);
	}

	public function delete()
	{
		foreach ($this->credentials as $cred)
		{
			$cred->image_id = null;
			$cred->save();
		}

		if ($this->hash)
		{
			if (is_dir($dir = \application\helpers\PathHelper::getResizesDirPath($this->hash)))
			{
				$iter = new DirectoryIterator($dir);

				foreach ($iter as $file) /** @var SplFileInfo $file */
				{
					if ($file->isFile())
					{
						unlink($file->getPathname());
					}
				}

				rmdir($dir);
			}
		}

		if ($this->file)
		{
			if (is_file($thumb = \application\helpers\PathHelper::getThumbnailFilePath($this->file)))
			{
				unlink($thumb);
			}

			if (is_file($file = \application\helpers\PathHelper::getImageFilePath($this->file)))
			{
				unlink($file);
			}
		}

		parent::delete();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('hash', $this->hash, true);
		$criteria->compare('file', $this->file, true);

		return new CActiveDataProvider($this, [
			'criteria' => $criteria,
		]);
	}
}
