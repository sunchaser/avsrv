<?php

class PasswordForm extends CFormModel
{
	public $password;
	public $new_password;
	public $confirmation;

	public function rules()
	{
		return [
			['password, new_password, confirmation', 'required'],
		];
	}

	public function verifyPassword(User $user)
	{
		$correct = true;

		if ($this->new_password !== $this->confirmation)
		{
			$this->addError('confirmation', 'New password does not match confirmation');
			$correct = false;
		}

		if ($user->checkPassword($this->password) === false)
		{
			$this->addError('password', 'Current password is invalid');
			$correct = false;
		}

		return $correct;
	}
}
