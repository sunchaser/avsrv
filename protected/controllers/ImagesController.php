<?php

/**
 * Work with images for the UI
 * (see AvatarController for the API)
 */
class ImagesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		];
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			['allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => ['index', 'view', 'image', 'thumbnail', 'create', 'update', 'delete'],
				'users'   => ['@'],
			],
			['allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => ['admin'],
				'users'   => ['admin'],
			],
			['deny', // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', [
			'model' => $this->loadModel($id),
		]);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Image;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Image']))
		{
			$model->attributes = $_POST['Image'];
			$model->image      = CUploadedFile::getInstance($model, 'image');

			$model->hash    = hash('sha256', time() . $model->image->name . $model->image->size);
			$model->file    = $model->hash . '.' . $model->image->extensionName;
			$model->user_id = Yii::app()->user->model->id;

			if ($model->save())
			{
				$model->image->saveAs(\application\helpers\PathHelper::getImageFilePath($model->file));
				$this->redirect(['view', 'id' => $model->id]);
			}
		}

		$this->render('create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Image']))
		{
			$model->attributes = $_POST['Image'];
			if ($model->save())
			{
				$this->redirect(['view', 'id' => $model->id]);
			}
		}

		$this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax']))
		{
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index']);
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('Image');
		$dataProvider->setPagination([
			'pageSize' => 60,
		]);
		$this->render('index', [
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new Image('search');
		$model->unsetAttributes(); // clear any default values
		if (isset($_GET['Image']))
		{
			$model->attributes = $_GET['Image'];
		}

		$this->render('admin', [
			'model' => $model,
		]);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Image the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Image::model()->findByPk($id);
		if ($model === null)
		{
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Image $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'image-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionImage($hash)
	{
		/** @var Image $img */
		$img = Image::model()->findByAttributes(['hash' => $hash]);

		if ($img === null)
		{
			throw new CHttpException('404', 'Image not found');
		}

		$file = \application\helpers\PathHelper::getImageFilePath($img->file);

		if (is_file($file) === false)
		{
			throw new CHttpException('500', 'Incorrect image');
		}

		header('Content-type: image');

		readfile($file);

		Yii::app()->end();
	}

	public function actionThumbnail($hash)
	{
		/** @var Image $img */
		$img = Image::model()->findByAttributes(['hash' => $hash]);

		if ($img === null)
		{
			throw new CHttpException('404', 'Image not found');
		}

		$file  = \application\helpers\PathHelper::getImageFilePath($img->file);
		$thumb = \application\helpers\PathHelper::getThumbnailFilePath($img->file);

		if (is_file($thumb) === false)
		{
			if (is_file($file) === false)
			{
				throw new CHttpException('500', 'Incorrect image');
			}

			\application\helpers\ImageResizer::Resize($file, $thumb, 80);
		}

		header('Expires: '. date('r', strtotime('+3 days')));
		header('Content-type: image/png');
		readfile($thumb);

		Yii::app()->end();
	}
}
