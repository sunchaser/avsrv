<?php

class AvatarController extends Controller
{
	public function actionAvatar($hash,
	                             $size = null,          $s = null,
	                             $default = null,       $d = null,
	                             $forcedefault = null,  $f = null
	) {
		$size = intval($size) ?: intval($s) ?: 80;
		$default = $default ?: $d;
		$forcedefault = ($forcedefault ?: $f);
		$forcedefault = ($forcedefault == '1' || $forcedefault == 'y');

		$type = null;

		if (strlen($hash) === 32)
		{
			$type = 'md5';
		}
		elseif (strlen($hash) === 64)
		{
			$type = 'sha256';
		}

		if ($type && $forcedefault === false)
		{
			/** @var Credential $cred */
			$cred = Credential::model()->findByAttributes([$type => $hash]);

			if ($cred && $cred->image_id)
			{
				$file = \application\helpers\PathHelper::getImageFilePath($cred->image->file);
				$avatar = \application\helpers\PathHelper::getResizeFilePath($cred->image->hash, $size);

				if (is_file($avatar) === false)
				{
					if (is_file($file) === false)
					{
						throw new CHttpException('500', 'Incorrect image');
					}

					\application\helpers\ImageResizer::Resize($file, $avatar, $size);
				}

				// enable cache for 3 days
				header('Cache: public');
				header('Expires: '. date('r', strtotime('+3 days')));
				header('Content-type: image/png');
				readfile($avatar);

				Yii::app()->end();
			}
		}

		$query = [
			's' => $size,
		];

		if ($default)
		{
			$query['d'] = $default;
		}

		if ($forcedefault)
		{
			$query['f'] = 'y';
			$hash = md5('test@example.com');
		}

		$ssl = !empty($_SERVER['HTTPS']);

		$link = $ssl ? 'http://cdn.libravatar.org' : 'https://seccdn.libravatar.org';
		$link.= '/avatar/'. $hash .'?'. http_build_query($query);

		$this->redirect($link, true);
	}
}
