<?php

class m130821_192408_demo extends CDbMigration
{
	public function safeUp()
	{
		$this->insert('users',['password' => 'pwd_admin']);
		$this->insert('credentials', [
			'value' => 'admin@example.com',
			'user_id' => new CDbExpression('(select max(id) from users)'),
			'type' => 1,
		]);
	}

	public function safeDown()
	{
		echo "m130821_192408_demo does not support migration down.\n";
		return false;
	}
}
