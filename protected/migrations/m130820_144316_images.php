<?php

class m130820_144316_images extends CDbMigration
{
	public function up()
	{
		$this->createTable('images', [
			'id' => 'serial not null primary key',
			'user_id' => 'integer not null references users',
			'hash' => 'varchar not null',
			'file' => 'varchar not null',
		]);

		$this->addColumn('credentials', 'image_id', 'int references images');
	}

	public function down()
	{
		$this->dropColumn('credentials', 'image_id');
		$this->dropTable('images');
	}
}
