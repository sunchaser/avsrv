<?php

class m130820_140024_create_user_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('users', [
			'id' => 'serial not null primary key',
			'password' => 'varchar not null',
			'reg_date' => 'timestamp with time zone not null default now()',
			'role' => 'integer not null default 1',
		]);

		$this->createTable('credentials', [
			'id' => 'serial not null primary key',
			'user_id' => 'integer not null references users',
			'value' => 'varchar not null',
			'type' => 'integer not null',
		]);
	}

	public function down()
	{
		$this->dropTable('credentials');
		$this->dropTable('users');
	}
}
