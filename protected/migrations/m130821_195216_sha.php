<?php

class m130821_195216_sha extends CDbMigration
{
	public function up()
	{
		$this->dropColumn('credentials', 'sha1');
		$this->addColumn('credentials', 'sha256', 'char(64)');
	}

	public function down()
	{
		$this->dropColumn('credentials', 'sha256');
		$this->addColumn('credentials', 'sha1', 'char(40)');
	}
}
