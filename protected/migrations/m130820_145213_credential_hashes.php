<?php

class m130820_145213_credential_hashes extends CDbMigration
{
	public function up()
	{
		$this->addColumn('credentials', 'md5', 'char(32)');
		$this->addColumn('credentials', 'sha1', 'char(40)');
	}

	public function down()
	{
		$this->dropColumn('credentials', 'md5');
		$this->dropColumn('credentials', 'sha1');
	}
}
