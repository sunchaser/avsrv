<?php

namespace application\helpers;

class PathHelper
{
	public static function getImageFilePath($filename)
	{
		return \Yii::app()->basePath . '/data/images/originals/'. $filename;
	}

	public static function getThumbnailFilePath($filename)
	{
		return \Yii::app()->basePath . '/data/images/thumbnails/'. $filename;
	}

	public static function getResizesDirPath($hash)
	{
		return \Yii::app()->basePath . '/data/images/resizes/'. $hash;
	}

	public static function getResizeFilePath($hash, $size)
	{
		return \Yii::app()->basePath . '/data/images/resizes/'. $hash .'/'.$size;
	}
} 
