<?php

namespace application\helpers;

class ImageResizer
{
	public static function Resize($src, $dst, $size)
	{
		$img = self::imagecreatefromfile($src);
		$resimg = imagecreatetruecolor($size, $size);

		// respect the alpha channel in png
		imagesavealpha($img, true);
		imagesavealpha($resimg, true);
		imagealphablending($resimg, false);

		$w = imagesx($img);
		$h = imagesy($img);
		$s = min($w, $h); // image will be cut by lowest dimension

		imagecopyresampled($resimg, $img, 0, 0, 0, 0, $size, $size, $s, $s);

		if (is_dir(dirname($dst)) === false)
		{
			mkdir(dirname($dst), 0777, true);
		}

		imagepng($resimg, $dst);
	}

	/**
	 * @param $filename
	 * @return resource
	 */
	private static function imagecreatefromfile($filename)
	{
		if (!file_exists($filename))
		{
			throw new \InvalidArgumentException('File "' . $filename . '" not found.');
		}

		switch (strtolower(pathinfo($filename, PATHINFO_EXTENSION)))
		{
			case 'jpeg':
			case 'jpg':
				return imagecreatefromjpeg($filename);
				break;

			case 'png':
				return imagecreatefrompng($filename);
				break;

			case 'gif':
				return imagecreatefromgif($filename);
				break;

			default:
				throw new \InvalidArgumentException('File "' . $filename . '" is not valid jpg, png or gif image.');
				break;
		}
	}
}
