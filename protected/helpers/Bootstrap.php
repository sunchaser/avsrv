<?php

namespace application\helpers;

/**
 * Non-Yii code to run on startup
 *
 * Class Bootstrap
 * @package application\helpers
 */
class Bootstrap
{
	public static function Bootstrap()
	{
		date_default_timezone_set(\Yii::app()->params['timezone']);
	}
}
